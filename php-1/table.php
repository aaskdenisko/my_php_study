﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
	<head>
		<title>Таблица умножения</title>
		<meta http-equiv="content-type"
			content="text/html; charset=windows-1251" />
		<link rel="stylesheet" type="text/css" href="style.css" />
	</head>
	<body>

		<div id="header">
			<!-- Верхняя часть страницы -->
			<img src="logo.gif" width="187" height="29" alt="Наш логотип" class="logo" />
			<span class="slogan">приходите к нам учиться</span>
			<!-- Верхняя часть страницы -->
		</div>

		<div id="content">
			<!-- Заголовок -->
			<h1>Таблица умножения</h1>
			<!-- Заголовок -->
			<!-- Область основного контента -->
			<form action='<?=SERVER?>'>
				<label>Количество колонок: </label><br />
				<input name='cols' type='text' value="" /><br />
				<label>Количество строк: </label><br />
				<input name='rows' type='text' value="" /><br />
				<label>Цвет: </label><br />
				<input name='color' type='text' value="" /><br /><br />
				<input type='submit' value='Создать' />
			</form>
			<!-- Таблица -->
			<?
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
			$cols = abs((int)$_POST['cols']);
			$rows = abs((int)$_POST['rows']);
			$color = trim(strip_tags($_POST['color']));
			}
			$cols = ($cols) ? $cols : 10;
			$rows = ($rows) ? $rows : 10;
			$color = ($color) ? $color : 'yellow';
			echo "<table border='1'>";			
				for ($x = 1 ; $x <= $rows ; $x++)		
			{
					echo "<tr>";
					for ($i = 1 ; $i <= $cols ; $i++)
					{
						if ($x ==  1 || $i == 1)
						echo "<th style='background-color : yellow'>" . $x * $i . "</th>";
						else
						echo "<td>" . $x * $i . "</td>";
					}
					echo "</tr>";
			}
			echo "</table>"	
			?>
			<!-- Таблица -->
			<!-- Область основного контента -->
		</div>
		<div id="nav">
			<h2>Навигация по сайту</h2>
			<!-- Меню -->
			<ul>
				<li><a href='index.php'>Домой</a></li>
				<li><a href='about.php'>О нас</a></li>
				<li><a href='contact.php'>Контакты</a></li>
				<li><a href='table.php'>Таблица умножения</a></li>
				<li><a href='calc.php'>Калькулятор</a></li>
			</ul>
			<!-- Меню -->
		</div>
		<div id="footer">
			<!-- Нижняя часть страницы -->
			&copy; Супер Мега Веб-мастер, 2000 - 2012
			<!-- Нижняя часть страницы -->
		</div>
	</body>
</html>